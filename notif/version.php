<?php


defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2017092500;             // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2017050500;             // Requires this Moodle version
$plugin->component = 'local_notif'; // Full name of the plugin (used for diagnostics)
