<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');
}

require_once($CFG->libdir . '/formslib.php');

class report_notif_form extends moodleform {

    public function definition() {
        global $CFG, $DB;
        $mform =& $this->_form;
        $courses = $this->get_course();

        $mform->addElement('select', 'course', get_string('course', 'local_notif'), $courses);


        $this->add_action_buttons(true, "Search");
    }

    public function validation($data, $files) {
        global $DB;
        $errors = parent::validation($data, $files);
   
        return $errors;
    }


    private function get_course(){
        $courses = get_courses(array('sort'=>'fullname'));

        $list_course = array();
        
        foreach ($courses as $key => $value) {
            $list_course[$value->id] = $value->fullname;

        }

        return $list_course;
    }
}
