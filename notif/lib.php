<?php
function get_notif($courseid)
{
	global $DB;

	$sql = '
			SELECT distinct u.id, u.username, cp.name,u.email, cc.courseid  FROM {competency_templatecomp} ct
			LEFT JOIN {competency_plan} cp on cp.templateid=ct.templateid
			LEFT JOIN {competency_coursecomp} cc on cc.competencyid=ct.competencyid
			JOIN {user} u on u.id = cp.userid
			where cc.courseid = :course  and cp.userid not in (select userid from {user_enrolments} ue join {enrol} e on e.id=ue.enrolid where courseid = :course2 )';

	$result = $DB->get_records_sql($sql, array('course' => $courseid,'course2' => $courseid));

	if(empty($result)){
		return false;
	}

	return $result;

}