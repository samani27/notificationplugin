<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');
}

require_once($CFG->libdir . '/formslib.php');

class report_notif_recipient_form extends moodleform {


    protected $list_student;

      public function __construct($action=null, $customdata=null, $method='post', $target='', $attributes=null, $editable=true,
                                $ajaxformdata=null, $list_student) {
            $this->list_student = $list_student;
            parent::__construct($action, $customdata, $method, $target, $attributes, $editable,
                                $ajaxformdata);

      }

    public function definition() {
        global $CFG, $DB;
        $mform =& $this->_form;
       

        $student = $this->list_student;
        
        reset($student);
        $first_key = key($student);
        $courseid = $student[$first_key]->courseid;
        $course = get_course($courseid);
        
        $mform->addElement("html", '<hr/><br/><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Edit Message</button><div id="demo" class="collapse">');
        $mform->addElement('text', 'subject', get_string('subject', 'local_notif'), array("style"=>'width:100%'));
        $mform->setDefault("subject",  $course->fullname.' is Open');
        $mform->setType('subject', PARAM_TEXT);
        
        $mform->addElement('editor', 'message', get_string('messagetext', 'local_notif'));
        $mform->setType('message', PARAM_RAW);
        $mform->setDefault('message', array('text' => $course->fullname.' is open, if you want to join this training, please click "Enrol me" button in the course page.'));
        $mform->addElement("html",'</div>');

        $mform->addElement('html', '<table class="table table-bordered table-stripped table-hover"><tr><th>#</th><th>Username</th><th>Learning Plan</th><th>Email</th><th>Send Notification</th></tr>');
        
        $i = 0;
        foreach ($student as $key => $value) {
            $mform->addElement("html", '<tr><td>'.(++$i).'</td><td>'.$value->username.'</td><td>'.$value->name.'</td><td>'.$value->email.'</td>');

            $mform->addElement("html", '<td>');
            $mform->addElement("advcheckbox", "recipient[".$value->id."]", '','',array(),array(0,$value->id));
            $mform->addElement("html", '</td></tr>');
            $mform->setDefault("recipient[".$value->id."]",$value->id); 
        }

        $mform->addElement("html", '</table>');
        
        $mform->addElement("hidden", 'courseid', $courseid);
        $mform->setType("courseid",  PARAM_INT);
      

        $this->add_action_buttons(true, "Send");
    }

    public function validation($data, $files) {
        global $DB;
        $errors = parent::validation($data, $files);
   
        return $errors;
    }


}
