<?php

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once(dirname(__FILE__) . '/report_form.php');
require_once(dirname(__FILE__) . '/recipient_form.php');
require_once(dirname(__FILE__) . '/lib.php');

require_login();
$context = context_system::instance();
require_capability('local/notif:view', $context);
$PAGE->set_context($context);
$mform = new report_notif_form();

admin_externalpage_setup('local_notif');
echo $OUTPUT->header();

echo html_writer::tag('h1',get_string('pluginname','local_notif'));
echo html_writer::empty_tag('br');

$mform->display();

$data = $mform->get_data();


if($data){
	echo html_writer::empty_tag('br');
	echo html_writer::empty_tag('br');
	echo html_writer::empty_tag('br');
	
	$result = get_notif($data->course);
	
	if($result){
		$rform = new report_notif_recipient_form(new moodle_url('/local/notif/send.php'), null, 'post',  '', null, true,null, $result);
		$rform->display();
	
		
	} else {
		echo html_writer::tag('p',"Data not found");
	}

} 



echo $OUTPUT->footer();
