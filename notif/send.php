<?php

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/messagelib.php');
require_once(dirname(__FILE__) . '/report_form.php');
require_once(dirname(__FILE__) . '/recipient_form.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once(dirname(__FILE__). '/../../message/lib.php');

require_login();
$context = context_system::instance();

$PAGE->set_url(new moodle_url('/local/notif/send.php'));
$PAGE->set_context($context);
$PAGE->set_title('Send Notificiation');
$PAGE->set_heading('Send Notification');
echo $OUTPUT->header();

$courseid = required_param('courseid', PARAM_INT);

$course = get_course($courseid);
$recipient = required_param_array('recipient', PARAM_INT);
$messagetext = required_param_array("message", PARAM_RAW);
$subject = required_param("subject", PARAM_TEXT);

foreach ($recipient as $key => $value) {

	if($value != 0){
	
		$userto = $DB->get_record('user', array('id' => $key));
		

		$message = new \core\message\message();
		$message->component = 'moodle';
		$message->name = 'instantmessage';
		$message->userfrom = $USER;
		$message->userto = $userto;
		$message->courseid= $courseid;
		$message->subject = $subject;
		$message->fullmessage = $messagetext['text'];
		$message->fullmessageformat = FORMAT_MARKDOWN;
		$message->fullmessagehtml = nl2br($messagetext['text']);
		$message->smallmessage = 'small message';
		$message->notification = '1';
		$message->contexturl = new moodle_url('/course/view.php?id='.$courseid);
		$message->contexturlname = 'Course link';
		$message->replyto = "random@example.com";
		$content = array('*' => array('header' => ' test ', 'footer' => ' test ')); // Extra content for specific processor
		$message->set_additional_content('email', $content);
		
		echo html_writer::tag('p',"Notification sent to ".$userto->username.' successfully');
		$messageid = message_send($message);		 
			
	}
	
}

var_dump($recipient);

echo $OUTPUT->footer();
